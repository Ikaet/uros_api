# UROS - Unreal ROS - API

FOR Advances on this project you can check [new-public-repository](https://gitlab.com/ikaet_public_projects/robotics/uros-api)
***

## Intro and Disclaimer

Looking to provide a prototype to implement differents technologies and make easier to learn and use ROS (Robotic Operating System) for people who may not necessarily knows all details related on how to establish a connection between ROS and Unreal Engine using websockets. This work is an example of implementation and postgraduate project (TFM) for Universidad Politécnica de Valencia, Spain, in september 2023.

For this prototype and initiative, i will not provide any support here about bugs or issues, but yes for continued development and collab. You are free to use this as a base or starting point for your own projects if you know how to use it. Nonetheless, I will do my best to keep the documentation and associated tutorials updated.

This is just a fast and simple minimum viable product; it cannot be considered a final product.

Anibal AG.
***

## v0.1 Concept Test

[YOUTUBE VIDEO UROS Concept Test](https://www.youtube.com/watch?v=WRTHt1Tl3xk)

[![UROS Concept Test](https://img.youtube.com/vi/WRTHt1Tl3xk/0.jpg)](https://www.youtube.com/watch?v=WRTHt1Tl3xk)

***

## What's Next

Actually im working on v1.0 to get a full integration product for virtual reality and integrate artificial intelligence in this project, create UDP and TCP sockets between other systems and implement open DDS for powerfull performance. Refactoring all this project to a clean architecture version too look below about roadmap and some advances in the new software design.

![UROS Roadmap](./imgs/UROS-Roadmap.svg){ width=100% max-width=512px display="block" margin="auto" }

![UROS Clean Architecture](./imgs/UROS-Architecture.svg){ width=100% max-width=512px display="block" margin="auto" }

![UROS Dev Enviroment Deploy](./imgs/UROS-Architecture-Deploy.svg){ width=100% max-width=512px display="block" margin="auto" }

***

## Refactoring Architecture Details

![UROS Architecture Domain Layer](./imgs/UROS-Architecture-Domain-Layer.svg){ width=100% max-width=512px display="block" margin="auto" }

![UROS Architecture App Layer](./imgs/UROS-Architecture-Application-Layer.svg){ width=100% max-width=512px display="block" margin="auto" }

![UROS Architecture External Layer](./imgs/UROS-Architecture-External-Layer.svg){ width=100% max-width=512px display="block" margin="auto" }

![UROS API](./imgs/UROS-Architecture-API.svg){ width=100% max-width=512px display="block" margin="auto" }

![UROS API PORTS ADAPTERS](./imgs/UROS-Architecture-API2.svg){ width=100% max-width=512px display="block" margin="auto" }

![UROS Operational Flow](./imgs/UROS-Architecture-Flow.svg){ width=100% max-width=512px display="block" margin="auto" }

***

## About License

This is currently a temporary MIT license. It will be updated in the future to explore financial sustainability and provide a robust tool for everyone in a free version, with a complete version available for business purposes.

***

## Tutorials

Work in progress

***
