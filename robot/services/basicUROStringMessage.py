# robot/services/basicUROStringMessage.py
from roslibpy import Topic
import time

def basicUROStringMessage(ros, uros_status_flag):
    # UrosPublisher for topic /basicUROStringMessage
    string_pub = Topic(ros, '/basicUROStringMessage', 'std_msgs/String')
    count = 0

    while uros_status_flag.is_set():
        message = {'data': f"Mensaje UROS ROS Humble #{count}"}
        string_pub.publish(message)
        count+=1
        time.sleep(0.5)
    
    message = {'data': f"terminar"}
    string_pub.publish(message)
    time.sleep(0.5)