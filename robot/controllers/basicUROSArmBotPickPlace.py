# robot/controllers/basicUROSArmBotPickPlace.py
from roslibpy import Topic
import time

def basicUROSArmBotPickPlace(ros, uros_status_flag):
    # UrosPublisher for topic /basicUROSArmBotPickPlace
    arm_publisher = Topic(ros, '/basicUROSArmBotPickPlace/joints', 'trajectory_msgs/JointTrajectory')
    gripper_publisher = Topic(ros, '/basicUROSArmBotPickPlace/gripper', 'std_msgs/Float64')

    # Home position TEST
    home_position = {
        'joint_names': ['joint_1', 'joint_2', 'joint_3', 'joint_4', 'joint_5', 'joint_6'],
        'points': [
            {
                'positions': [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                'velocities': [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                'time_from_start': {'secs': 1, 'nsecs': 0}
            }
        ]
    }
    # Pick position TEST
    pick_position = {
        'joint_names': ['joint_1', 'joint_2', 'joint_3', 'joint_4', 'joint_5', 'joint_6'],
        'points': [
            {
                'positions': [1.0, 0.5, -0.5, 0.0, 0.0, 0.0],
                'velocities': [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                'time_from_start': {'secs': 1, 'nsecs': 0}
            }
        ]
    }
    # Place position TEST
    place_position = {
        'joint_names': ['joint_1', 'joint_2', 'joint_3', 'joint_4', 'joint_5', 'joint_6'],
        'points': [
            {
                'positions': [0.5, -0.2, 0.3, 0.0, 0.0, 0.0],
                'velocities': [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                'time_from_start': {'secs': 1, 'nsecs': 0}
            }
        ]
    }
    
    while uros_status_flag.is_set():
        # Move to home position
        arm_publisher.publish(home_position)
        time.sleep(3)

        # Move to pick
        arm_publisher.publish(pick_position)
        time.sleep(3)

        # Activate gripper
        gripper_publisher.publish({'data': 0.5})
        time.sleep(1)
            
        # Move to place
        arm_publisher.publish(place_position)
        time.sleep(3)

        # Deactivate gripper
        gripper_publisher.publish({'data': 0.0})
        time.sleep(1)