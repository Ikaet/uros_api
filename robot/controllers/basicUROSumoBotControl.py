# robot/controllers/basicUROSumoBotControl.py
from roslibpy import Topic, Message
import time

def basicUROSumoBotControl(ros, uros_status_flag):
    # UrosPublisher for topic /basicUROSumoBotControl
    cmd_vel_publisher = Topic(ros, '/basicUROSumoBotControl', 'geometry_msgs/Twist')

    twist_msg = Message({
        'linear': {'x': 0.0, 'y': 0.0, 'z': 0.0},
        'angular': {'x': 0.0, 'y': 0.0, 'z': 0.0}
    })

    while uros_status_flag.is_set():
        twist_msg['linear']['x'] = 0.0
        twist_msg['linear']['y'] = 0.0
        twist_msg['linear']['z'] = 0.0
        twist_msg['angular']['x'] = 0.0
        twist_msg['angular']['y'] = 0.0
        twist_msg['angular']['z'] = 0.0
        cmd_vel_publisher.publish(twist_msg)
        time.sleep(4)
            
        twist_msg['linear']['x'] = 0.0
        twist_msg['linear']['y'] = -0.5
        twist_msg['linear']['z'] = 0.0
        twist_msg['angular']['x'] = 0.0
        twist_msg['angular']['y'] = 0.0
        twist_msg['angular']['z'] = 0.0
        cmd_vel_publisher.publish(twist_msg)
        time.sleep(3)
        
        twist_msg['linear']['x'] = 0.0
        twist_msg['linear']['y'] = 0.0
        twist_msg['linear']['z'] = 0.0
        twist_msg['angular']['x'] = 0.0
        twist_msg['angular']['y'] = 0.0
        twist_msg['angular']['z'] = 0.0
        cmd_vel_publisher.publish(twist_msg)
        time.sleep(2)
        
        twist_msg['linear']['x'] = 0.0
        twist_msg['linear']['y'] = -0.5
        twist_msg['linear']['z'] = 0.0
        twist_msg['angular']['x'] = 0.0
        twist_msg['angular']['y'] = 0.0
        twist_msg['angular']['z'] = -1.0
        cmd_vel_publisher.publish(twist_msg)
        time.sleep(2)
        
        twist_msg['linear']['x'] = 0.0
        twist_msg['linear']['y'] = 0.0
        twist_msg['linear']['z'] = 0.0
        twist_msg['angular']['x'] = 0.0
        twist_msg['angular']['y'] = 0.0
        twist_msg['angular']['z'] = 0.0
        cmd_vel_publisher.publish(twist_msg)
        time.sleep(2)
        
        twist_msg['linear']['x'] = 0.0
        twist_msg['linear']['y'] = -0.5
        twist_msg['linear']['z'] = 0.0
        twist_msg['angular']['x'] = 0.0
        twist_msg['angular']['y'] = 0.0
        twist_msg['angular']['z'] = 0.0
        cmd_vel_publisher.publish(twist_msg)
        time.sleep(3)
        
        twist_msg['linear']['x'] = 0.0
        twist_msg['linear']['y'] = 0.0
        twist_msg['linear']['z'] = 0.0
        twist_msg['angular']['x'] = 0.0
        twist_msg['angular']['y'] = 0.0
        twist_msg['angular']['z'] = 0.0
        cmd_vel_publisher.publish(twist_msg)
        time.sleep(2)
        
        twist_msg['linear']['x'] = 0.0
        twist_msg['linear']['y'] = -0.5
        twist_msg['linear']['z'] = 0.0
        twist_msg['angular']['x'] = 0.0
        twist_msg['angular']['y'] = 0.0
        twist_msg['angular']['z'] = -1.0
        cmd_vel_publisher.publish(twist_msg)
        time.sleep(2)
        
        twist_msg['linear']['x'] = 0.0
        twist_msg['linear']['y'] = 0.0
        twist_msg['linear']['z'] = 0.0
        twist_msg['angular']['x'] = 0.0
        twist_msg['angular']['y'] = 0.0
        twist_msg['angular']['z'] = 0.0
        cmd_vel_publisher.publish(twist_msg)
        time.sleep(2)
        
        twist_msg['linear']['x'] = 0.0
        twist_msg['linear']['y'] = -0.5
        twist_msg['linear']['z'] = 0.0
        twist_msg['angular']['x'] = 0.0
        twist_msg['angular']['y'] = 0.0
        twist_msg['angular']['z'] = 0.0
        cmd_vel_publisher.publish(twist_msg)
        time.sleep(3)
        
        twist_msg['linear']['x'] = 0.0
        twist_msg['linear']['y'] = 0.0
        twist_msg['linear']['z'] = 0.0
        twist_msg['angular']['x'] = 0.0
        twist_msg['angular']['y'] = 0.0
        twist_msg['angular']['z'] = 0.0
        cmd_vel_publisher.publish(twist_msg)
        time.sleep(4)