import roslibpy
import keyboard

def main():
    # WebSocket Connection
    client = roslibpy.Ros(host='localhost', port=9091)
    client.run()

    # UrosPublisher for topic /basicUROSBotTeleop
    cmd_vel_publisher = roslibpy.Topic(client, '/basicUROSBotTeleop', 'geometry_msgs/Twist')

    twist_msg = roslibpy.Message({
        'linear': {'x': 0.0, 'y': 0.0, 'z': 0.0},
        'angular': {'x': 0.0, 'y': 0.0, 'z': 0.0}
    })

    def on_key_event(e):
        twist_msg['linear']['x'] = 0.0
        twist_msg['linear']['y'] = 0.0
        twist_msg['linear']['z'] = 0.0
        twist_msg['angular']['x'] = 0.0
        twist_msg['angular']['y'] = 0.0
        twist_msg['angular']['z'] = 0.0
        
        if e.event_type == keyboard.KEY_DOWN:
            
            if e.name == 'w':
                twist_msg['linear']['x'] = 1.0
            elif e.name == 's':
                twist_msg['linear']['x'] = -1.0
            elif e.name == 'd':
                twist_msg['angular']['z'] = -1.0
            elif e.name == 'a':
                twist_msg['angular']['z'] = 1.0

            if (
                twist_msg['linear']['x'] != 0.0 or
                twist_msg['linear']['y'] != 0.0 or
                twist_msg['linear']['z'] != 0.0 or
                twist_msg['angular']['x'] != 0.0 or
                twist_msg['angular']['y'] != 0.0 or
                twist_msg['angular']['z'] != 0.0
            ):
                cmd_vel_publisher.publish(twist_msg)

        if e.name == 'space' and e.event_type == keyboard.KEY_DOWN:
            # UROS Controller Custom Future Functionality
            pass

        if e.event_type == keyboard.KEY_UP:
            
            if e.name == 'w' or e.name == 's' or e.name == 'a' or e.name == 'd':
                if e.name == 'w' or e.name == 's':
                    twist_msg['linear']['x'] = 0.0
                elif e.name == 'a' or e.name == 'd':
                    twist_msg['angular']['z'] = 0.0
                cmd_vel_publisher.publish(twist_msg)
            else:
                pass

    keyboard.hook(on_key_event)
    keyboard.wait()

if __name__ == '__main__':
    main()
