import os, logging, uvicorn, json, re, threading, time
from fastapi import FastAPI, HTTPException, WebSocket, WebSocketDisconnect, Request
from websockets.exceptions import ConnectionClosedError
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from roslibpy import Ros, Topic, Message
from typing import Set, Dict
from pymongo import MongoClient

from robot.services.basicUROStringMessage import basicUROStringMessage
from robot.controllers.basicUROSumoBotControl import basicUROSumoBotControl
from robot.controllers.basicUROSArmBotPickPlace import basicUROSArmBotPickPlace 

# UROS API version
api_version = "version 0.1"
description = """
# This API is develop for Master's Degree Project
## "Desarrollo de un Laboratorio Virtual de Robots Utilizando un Motor Gráfico para Videojuegos"

***

### Author: Anibal Atahualpa Guerrero
### Master Universitario en Automática e Informática Industrial
### Universidad Politécnica de Valencia - UPV

***
#### This implements roslibpy to access and interact with ROS and ROS 2 servers using websockets
#### This implements websockets to connect for UnrealEngine 5.2

### For now you can:

* Connect/disconnect to ROS in real time
* Subscribe/unsubscribe to ROS Topics and save in json format
* Subscribe/unsubscribe to ROS Topics and save data in MongoDB
* Subscribe to ROS Topics and get for Unreal Engine 5.2 Connection
"""

app = FastAPI(
    title= "UROS API",
    description= description,
    version= api_version
)

# Mongo DB basic client setup
mongo_client = MongoClient('mongodb://root:root123@localhost:27019/?authMechanism=DEFAULT')
db = mongo_client['uros_api']

#Use Uvicorn Logger
uvicorn_logger = logging.getLogger("uvicorn")

# Setup middleware CORS
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

#ROS Simple
ros_connections: Dict[str, Ros] = {}  # Dict conn ROS
ros_topics: Dict[str, Topic] = {} # Dict topic ROS

# WEBSOCKETS simple
ws_active_connections: Set[WebSocket] = set()
message_recv_ros = "" #SaveTemporal VAR

#Threads Simple
publishing_semaphore = threading.Semaphore()
uros_threads = []
ros_topic_publishing_status = False #Bool to publish data

#Controllers Threads
uros_status_flag = threading.Event()

# Get current path
current_directory = os.getcwd()

def is_valid_ip(ip):
    if ip == "localhost":
        return True
    ip_pattern = r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$'
    return bool(re.match(ip_pattern, ip))

def is_valid_port(port):
    port_pattern = r'^(?:[1-9]\d{0,4}|[1-5]\d{4}|6[0-5][0-5][0-3][0-5])$'
    return bool(re.match(port_pattern, str(port)))

def ros_begin_connection(ip, port):
    uvicorn_logger.info(f"UROSConnection Host: {ip} Port: {port}")
    try:
        if "single_ros_server" not in ros_connections:
            ros = Ros(host=ip, port=port)
            ros_connections["single_ros_server"] = ros
            ros.run()
            uvicorn_logger.info(f"UROSConnection open on {ip}:{port}")
            return "success", None
        else:
            ros = ros_connections["single_ros_server"]
            ros.connect()
            uvicorn_logger.info(f"UROSConnection reopen on {ip}:{port}")
            return "already exist - restarted", None
    
    except Exception as e:
        uvicorn_logger.error(f"Error UROS creating ROS connection: {str(e)}")
        return "ERROR", str(e)

def ros_end_connection():
    ros = ros_connections.get("single_ros_server")
    
    if ros and ros.is_connected:
        try:
            ros.close()
            uvicorn_logger.info(f"UROSConnection closed")
            return "success", None
        except Exception as e:
            uvicorn_logger.error(f"Error UROS closing ROS connection: {str(e)}")
            return "ERROR", str(e)
    return "no active connection", None

def ros_message_callback(message, topic_name, message_type):
    json_message = json.dumps(message)
    
    if topic_name.startswith('/'):
        topic_name_path = topic_name[1:]
        
    # Get current path
    file_path = os.path.join(f'{current_directory}', 'temp/', f'{topic_name_path}.json')
    
    dir_path = os.path.dirname(file_path)
    os.makedirs(dir_path, exist_ok=True)
    
    # if File exist
    mode = "w" if not os.path.exists(file_path) else "a"
    
    # Save message in JSON
    with open(file_path, mode) as file:
        file.write(json_message + "\n")

def ros_subscribe(ros, topic_name, message_type):
    if topic_name in ros_topics:
        return "already exist", None
    
    new_topic = Topic(ros, topic_name, message_type)
    ros_topics[topic_name] = new_topic
    
    new_topic.subscribe(lambda message, topic_name=topic_name: ros_message_callback(message, topic_name, message_type))
    uvicorn_logger.info(f"UROS_Subscription Topic: {topic_name} MessageType: {message_type}")

    return "success", None

def ros_unsubscribe(topic_name):
    if not topic_name in ros_topics:
        return "no exist", None
    
    unsubscribe_topic = ros_topics[topic_name]
    unsubscribe_topic.unsubscribe()
    
    del ros_topics[topic_name]
    
    uvicorn_logger.info(f"UROS_Unsubscription Topic: {topic_name}")
    return "success", None

def ros_publish(ros, topic_name, message_type, message_data):
    global ros_topic_publishing_status
    global uros_threads
    
    if topic_name in ros_topics:
        return "already exist", None
    
    new_topic = Topic(ros, topic_name, message_type)
    ros_topics[topic_name] = new_topic
    uvicorn_logger.info(f"UROS_Publish Topic: {topic_name} MessageType: {message_type}")
    
    if message_type == "std_msgs/String":
        message_data = {'data': message_data}
    elif message_type == "std_msgs/Float64":
        message_data = {'data': float(message_data)}
    elif message_type == "geometry_msgs/Twist":
        #Format first part linear second angular(1,2,3)(4,5,6)
        parts = message_data.strip('()').split(')(')
        
        if len(parts) == 2:
            linear_values = list(map(float, parts[0].split(',')))
            angular_values = list(map(float, parts[1].split(',')))
            
            message_data = {
                'linear': {'x': linear_values[0], 'y': linear_values[1], 'z': linear_values[2]},
                'angular': {'x': angular_values[0], 'y': angular_values[1], 'z': angular_values[2]}
            }
        else:
            raise ValueError("Invalid format for geometry_msgs/Twist")
    elif message_type == "trajectory_msgs/JointTrajectory":
        # Temporal format message 
        # numberOfJoints(positionJoint1,positionJoint2,...positionJointn)(velJoint1,velJoint2,...velJointn)(timestartsec,timenanosec)
        num_joints_str = message_data.split('(')[0]
        num_joints = int(num_joints_str)
        positions_str = message_data.split('(')[1].split(')')[0]
        velocities_str = message_data.split('(')[2].split(')')[0]
        time_from_start_str = message_data.split('(')[3].split(')')[0]

        positions = list(map(float, positions_str.split(',')))
        velocities = list(map(float, velocities_str.split(',')))
        time_from_start_parts = time_from_start_str.split(',')
        
        time_from_start = {
            'secs': int(time_from_start_parts[0]),
            'nsecs': int(time_from_start_parts[1]) if len(time_from_start_parts) >= 2 else 0
        }
        
        message_data = {
            'joint_names': [f'joint_{i}' for i in range(1, num_joints + 1)],
            'points': [
                {
                    'positions': positions,
                    'velocities': velocities,
                    'time_from_start': time_from_start
                }
            ]
        }
    
    message = Message(message_data)
    
    ros_topic_publishing_status = True
    
    publish_thread = threading.Thread(target=publish_message_in_thread, args=(new_topic, message))
    publish_thread.start()
    uros_threads.append(publish_thread)
    
    return "succes", None

def publish_message_in_thread(topic, message):
    global ros_topic_publishing_status
    while ros_topic_publishing_status:
        time.sleep(0.2)
        with publishing_semaphore:
            topic.publish(message)

def stop_publishing():
    global ros_topic_publishing_status
    ros_topic_publishing_status = False
    publishing_semaphore.release()

def ros_save_to_mongodb_callback(message, topic_name):
    #json_message = json.dumps(message)
    db[topic_name].insert_one(message)

def ros_subscribe_and_save_to_mongodb(ros, topic_name, message_type):
    if not topic_name in ros_topics:
        new_topic = Topic(ros, topic_name, message_type)
        ros_topics[topic_name] = new_topic

    topic_to_save = ros_topics[topic_name]
    topic_to_save.subscribe(lambda message, topic_name=topic_name: ros_save_to_mongodb_callback(message, topic_name))
    
    uvicorn_logger.info(f"UROS_Subscription_SaveMongoDB Topic: {topic_name} MessageType: {message_type}")
    return "success", None

def on_shutdown():
    global uros_status_flag
    
    uvicorn_logger.info(f"UROS API - {api_version} on shutdown protocol")
    uros_status_flag.clear()
    
    # Terminate ROS connection
    if "single_ros_server" in ros_connections:
        ros = ros_connections["single_ros_server"]
        ros.terminate()

def uros_controllers_run(ros, robot_control):
    global uros_threads
    global uros_status_flag
    
    uros_status_flag.set()
    
    uvicorn_logger.info(f"UROS Running - {robot_control}")
    
    if robot_control == "basicUROStringMessage":
        publish_thread = threading.Thread(target=basicUROStringMessage, args=(ros, uros_status_flag))
        publish_thread.start()
        uros_threads.append(publish_thread)
        return "succes", None
    
    elif robot_control == "basicUROSumoBotControl":
        publish_thread = threading.Thread(target=basicUROSumoBotControl, args=(ros, uros_status_flag))
        publish_thread.start()
        uros_threads.append(publish_thread)
        
        return "succes", None
    
    elif robot_control == "basicUROSArmBotPickPlace":        
        publish_thread = threading.Thread(target=basicUROSArmBotPickPlace, args=(ros, uros_status_flag))
        publish_thread.start()
        uros_threads.append(publish_thread)
        
        return "succes", None
    else:
        return "Controller not Exist", None

def uros_controllers_stop(robot_control):
    uvicorn_logger.info(f"UROS Stopping - {robot_control}")
    global uros_status_flag
    
    uros_status_flag.clear()
    
    if robot_control == "basicUROStringMessage":
        topic_name = "/basicUROStringMessage"
        ros_unsubscribe(topic_name)
        return "succes", None
    elif robot_control == "basicUROSumoBotControl":
        topic_name = "/basicUROSumoBotControl"
        ros_unsubscribe(topic_name)
        return "succes", None
    elif robot_control == "basicUROSArmBotPickPlace":
        topic_name = "/basicUROSArmBotPickPlace/joints"
        topic_name2 = "/basicUROSArmBotPickPlace/gripper"
        ros_unsubscribe(topic_name)
        ros_unsubscribe(topic_name2)
        return "succes", None
    else: 
        return "Controller not Exist", None

def uros_controllers_save(ros, robot_control):
    if uros_status_flag.is_set():
        if robot_control == "basicUROStringMessage":
            topic_name = "/basicUROStringMessage"
            message_type = "std_msgs/String"
            ros_subscribe_and_save_to_mongodb(ros, topic_name, message_type)
            
        elif robot_control == "basicUROSumoBotControl":
            topic_name = "/basicUROSumoBotControl"
            message_type = "geometry_msgs/Twist"
            ros_subscribe_and_save_to_mongodb(ros, topic_name, message_type)
            
        elif robot_control == "basicUROSArmBotPickPlace":
            topic_name = "/basicUROSArmBotPickPlace/joints"
            topic_name2 = "/basicUROSArmBotPickPlace/gripper"
            message_type = "trajectory_msgs/JointTrajectory"
            message_type2 = "std_msgs/Float64"
            ros_subscribe_and_save_to_mongodb(ros, topic_name, message_type)
            ros_subscribe_and_save_to_mongodb(ros, topic_name2, message_type2)
    else:
        return "Not running controllers", None
    return "success", None

def uros_unreal_message_callback(message, topic_name):
    global message_recv_ros
    message_recv_ros = json.dumps(message)
    #message_recv_ros = message['data']

@app.post("/uros/connect")
async def connect_to_ros_server(request: Request):
    data = await request.json()
    
    ip = data.get("ip")
    port = data.get("port")

    if not is_valid_ip(ip) or not is_valid_port(port):
        raise HTTPException(status_code=400, detail="Se requiere una IP válida y un puerto válido para conectarse a ROS")
    
    # Continúa con tu lógica de conexión a ROS
    status, err = ros_begin_connection(ip, int(port))
    
    if status == "ERROR":
        return {"message": f"{status} {err}"}
    
    return {"message": f"Conexión con ROS {status} en {ip}:{port}"}

@app.post("/uros/disconnect")
async def disconnect_to_ros_server(request: Request):
    data = await request.json()
    
    ip = data.get("ip")
    port = data.get("port")

    if not ip or not port:
        raise HTTPException(status_code=400, detail="Se requiere IP, y puerto para desconectarse de ROS")
    
    status, err = ros_end_connection()
    
    if status == "ERROR":
        return {"message": f"{status} {err}"}
    
    return {"message": f"Desconexión con ROS {status} en {ip}:{port}"}

@app.post("/uros/subscribe")
async def subscribe_to_ros_topic(request: Request):
    data = await request.json()
    topic_name = data.get("topic")
    message_type = data.get("message_type")
    
    if not topic_name or not message_type:
        raise HTTPException(status_code=400, detail="Se requiere nombre y tipo del tópico.")
    
    if not "single_ros_server" in ros_connections:
        raise HTTPException(status_code=400, detail="No hay conexion activa con ROS")
    
    ros = ros_connections["single_ros_server"]
    status, err = ros_subscribe(ros, topic_name, message_type)
    
    if status == "ERROR":
        return {"message": f"{status} {err}"}
    
    return {"message": f"Suscripción a {topic_name} tipo {message_type} {status}"}

@app.post("/uros/unsubscribe")
async def unsubscribe_from_ros_topic(request: Request):
    data = await request.json()
    topic_name = data.get("topic")

    if not topic_name or topic_name not in ros_topics:
        raise HTTPException(status_code=400, detail="Tópico no válido o no suscrito.")
    
    status, err = ros_unsubscribe(None, topic_name)

    return {"message": f"Desuscripción a {topic_name} {status}"}

@app.post("/uros/publish")
async def publish_to_ros_topic(request: Request):
    data = await request.json()
    topic_name = data.get("topic")
    message_type = data.get("message_type")
    message_data = data.get("message_data")
    
    if not topic_name or not message_type or not message_data:
        raise HTTPException(status_code=400, detail="Se requiere nombre, tipo y datos para publicar el tópico.")
    
    if not "single_ros_server" in ros_connections:
        raise HTTPException(status_code=400, detail="No hay conexion activa con ROS")
    
    ros = ros_connections["single_ros_server"]
    status, err = ros_publish(ros, topic_name, message_type, message_data)
    
    if status == "ERROR":
        return {"message": f"{status} {err}"}
    
    return {"message": f"Publicando en {topic_name} tipo {message_type} {status}"}

@app.post("/uros/unpublish")
async def publish_to_ros_topic():
    if not "single_ros_server" in ros_connections:
        raise HTTPException(status_code=400, detail="No hay conexion activa con ROS")
    
    stop_publishing()
    return {"message": f"Dejó de publicar"}
    
@app.post("/uros/mongodb/save")
async def subscribe_and_save_to_mongodb(request: Request):
    data = await request.json()
    topic_name = data.get("topic")
    message_type = data.get("message_type")

    if not topic_name or not message_type:
        raise HTTPException(status_code=400, detail="Se requiere nombre y tipo del tópico.")

    if not "single_ros_server" in ros_connections:
        raise HTTPException(status_code=400, detail="No hay conexion activa con ROS")

    ros = ros_connections["single_ros_server"]
    status, err = ros_subscribe_and_save_to_mongodb(ros, topic_name, message_type)

    if status == "ERROR":
        return {"message": f"{status} {err}"}

    return {"message": f"Suscripción y guardado en MongoDB a {topic_name} tipo {message_type} {status}"}

@app.get("/uros/mongodb/data")
async def get_mongodb_data(topic_name: str, limit: int = 5):
    topic_name = topic_name.strip("'")
    uvicorn_logger.info(f"UROS_MongoDB Get Data Topic: {topic_name}")
    
    collection = db[topic_name]
    if db.get_collection(topic_name) is None:
        raise HTTPException(status_code=400, detail="Tópico no encontrado en la base de datos")

    data = []

    query_result = collection.find().limit(limit)  # Simple Query
    
    for document in query_result:
        # ObjectId conversion to string before add to list
        document['_id'] = str(document['_id'])
        data.append(document)  # Add docs to list
        
    return JSONResponse(content=data)

@app.post("/uros/controllers/run")
async def run_robot_controller(request: Request):
    data = await request.json()
    robot_control = data.get("robot_control")
    
    if not "single_ros_server" in ros_connections:
        raise HTTPException(status_code=400, detail="No hay conexion activa con ROS")
    
    ros = ros_connections["single_ros_server"]
    
    status, err = uros_controllers_run(ros, robot_control)
    
    if status == "ERROR":
        return {"message": f"{status} {err}"}
    
    return {"message": f"Corriendo {robot_control} {status}"}

@app.post("/uros/controllers/stop")
async def stop_robot_controller(request: Request):
    data = await request.json()
    robot_control = data.get("robot_control")
    
    if not "single_ros_server" in ros_connections:
        raise HTTPException(status_code=400, detail="No hay conexion activa con ROS")
    
    status, err = uros_controllers_stop(robot_control)
    
    if status == "ERROR":
        return {"message": f"{status} {err}"}
    
    return {"message": f"Solicistaste Parar {robot_control} {status}"}

@app.post("/uros/controllers/save")
async def save_robot_controller(request: Request):
    data = await request.json()
    robot_control = data.get("robot_control")
    
    if not "single_ros_server" in ros_connections:
        raise HTTPException(status_code=400, detail="No hay conexion activa con ROS")
    
    ros = ros_connections["single_ros_server"]
    
    status, err = uros_controllers_save(ros, robot_control)
    
    if status == "ERROR":
        return {"message": f"{status} {err}"}
    
    return {"message": f"Solicistaste Guardar {robot_control} {status}"}

@app.websocket("/uros/unreal/ws")
async def unreal_ros_websocket(websocket: WebSocket):
    await websocket.accept()
    
    ip = websocket.client.host
    port = websocket.client.port
    
    uvicorn_logger.info(f"UROSWebsocket Unreal open {ip}:{port}")
    
    message_sent_ws = "Hello from UROS API"
    message_recv_ws = ""
    count = 3
    
    try:
        while True:
            message_recv_ws = await websocket.receive_text()
            
            if message_recv_ws != "":
                print(f"Mensaje recibido: {message_recv_ws}")
                await websocket.send_text(message_sent_ws)
            
            if count >=3:
                message_sent_ws = "terminar"
                await websocket.send_text(message_sent_ws)
                
            count=+1
            
    except WebSocketDisconnect:
        uvicorn_logger.info("UROSWebsocket Unreal closed")

@app.websocket("/uros/unreal/ws/test/string")
async def unreal_ros_websocket(websocket: WebSocket):
    await websocket.accept()
    global message_recv_ros
    
    message_recv_ws = ""
    
    ip = websocket.client.host
    port = websocket.client.port
    
    uvicorn_logger.info(f"UROSWebsocket Unreal Test String Open {ip}:{port}")
    
    message_sent_ws = "Iniciando Suscripcion ROS Topico /basicUROStringMessage"
    
    if not "single_ros_server" in ros_connections:
        await websocket.send_text("No hay conexion activa con ROS")
    
    ros = ros_connections["single_ros_server"]
    
    topic_name = "/basicUROStringMessage"
    message_type = "std_msgs/String"
    
    if not topic_name in ros_topics:
        new_topic = Topic(ros, topic_name, message_type)
        ros_topics[topic_name] = new_topic

    topic_to_unreal = ros_topics[topic_name]
    await websocket.send_text(message_sent_ws)
    topic_to_unreal.subscribe(lambda message, topic_name=topic_name: uros_unreal_message_callback(message, topic_name))
    
    try:
        while True:
            
            #message_recv_ws = await websocket.receive_text()
    
            #if message_recv_ws == "UROSim UE5.2 Connected":
            #    print(f"Mensaje recibido: {message_recv_ws}")
            #    await websocket.send_text(message_sent_ws)

            if message_recv_ros:
                await websocket.send_text(message_recv_ros)
                message_recv_ros = ""
            
    except WebSocketDisconnect:
        uvicorn_logger.info("UROSWebsocket Unreal closed")
        topic_to_unreal.unsubscribe()
        
    except ConnectionClosedError as e:
        if "no close frame received" in str(e):
            uvicorn_logger.info("UROSWebsocket Unreal closed (no close frame received)")
        else:
            uvicorn_logger.error("Error de conexión en el WebSocket:", e)
    topic_to_unreal.unsubscribe()

@app.websocket("/uros/unreal/ws/robot/sumo")
async def unreal_ros_websocket(websocket: WebSocket):
    await websocket.accept()
    global message_recv_ros
    
    ip = websocket.client.host
    port = websocket.client.port
    
    uvicorn_logger.info(f"UROSWebsocket Unreal Robot Sumo Open {ip}:{port}")
    message_sent_ws = "Iniciando Suscripcion ROS Topico /basicUROSumoBotControl"
    
    if not "single_ros_server" in ros_connections:
        await websocket.send_text("No hay conexion activa con ROS")
    
    ros = ros_connections["single_ros_server"]
    
    topic_name = "/basicUROSumoBotControl"
    message_type = "geometry_msgs/Twist"
    
    if not topic_name in ros_topics:
        new_topic = Topic(ros, topic_name, message_type)
        ros_topics[topic_name] = new_topic

    topic_to_unreal = ros_topics[topic_name]
    await websocket.send_text(message_sent_ws)
    topic_to_unreal.subscribe(lambda message, topic_name=topic_name: uros_unreal_message_callback(message, topic_name))
    
    try:
        while True:

            if message_recv_ros:
                await websocket.send_text(message_recv_ros)
                message_recv_ros = ""
            
    except WebSocketDisconnect:
        uvicorn_logger.info("UROSWebsocket Unreal closed")
        topic_to_unreal.unsubscribe()
        
    except ConnectionClosedError as e:
        if "no close frame received" in str(e):
            uvicorn_logger.info("UROSWebsocket Unreal closed (no close frame received)")
        else:
            uvicorn_logger.error("Error de conexión en el WebSocket:", e)
    topic_to_unreal.unsubscribe()
    
@app.websocket("/uros/unreal/ws/robot/6r")
async def unreal_ros_websocket(websocket: WebSocket):
    await websocket.accept()
    global message_recv_ros
    
    ip = websocket.client.host
    port = websocket.client.port
    
    uvicorn_logger.info(f"UROSWebsocket Unreal Robot Kuka KR16 Open {ip}:{port}")
    message_sent_ws = "Iniciando Suscripcion ROS Topico /basicUROSArmBotPickPlace/joints"
    
    if not "single_ros_server" in ros_connections:
        await websocket.send_text("No hay conexion activa con ROS")
    
    ros = ros_connections["single_ros_server"]
    
    topic_name = "/basicUROSArmBotPickPlace/joints"
    message_type = "trajectory_msgs/JointTrajectory"
    
    if not topic_name in ros_topics:
        new_topic = Topic(ros, topic_name, message_type)
        ros_topics[topic_name] = new_topic

    topic_to_unreal = ros_topics[topic_name]
    await websocket.send_text(message_sent_ws)
    topic_to_unreal.subscribe(lambda message, topic_name=topic_name: uros_unreal_message_callback(message, topic_name))
    
    try:
        while True:

            if message_recv_ros:
                await websocket.send_text(message_recv_ros)
                message_recv_ros = ""
            
    except WebSocketDisconnect:
        uvicorn_logger.info("UROSWebsocket Unreal closed")
        topic_to_unreal.unsubscribe()
        
    except ConnectionClosedError as e:
        if "no close frame received" in str(e):
            uvicorn_logger.info("UROSWebsocket Unreal closed (no close frame received)")
        else:
            uvicorn_logger.error("Error de conexión en el WebSocket:", e)
    topic_to_unreal.unsubscribe()

@app.on_event("startup")
async def startup_event():
    uvicorn_logger.info(f"UROS API - {api_version} on startup Protocol")

@app.on_event("shutdown")
async def shutdown_event():
    on_shutdown()

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
